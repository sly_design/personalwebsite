module.exports = {
  purge: {
    enabled: true,
    mode: 'all',
    preserveHtmlElements: false,
    content: ['dist/**/*.html'],
    options: {
      keyframes: true,
      fontFace: true,
    },
    },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    debugScreens: {
      position: ['top', 'left'],
    },
    screens: {
      'mobile': '576px',
      // => @media (min-width: 576px) { ... }

      'tablet': '768px',
      // => @media (min-width: 768px) { ... }

      'desktop': '992px',
      // => @media (min-width: 1200px) { ... }
    },
    extend: {
      fontFamily: {
      primary: ['Crimson Text', 'ui-serif', 'serif', 'Times New Roman'],
      secundary: ['Lato', 'ui-sans-serif', 'sans-serif', 'Arial']
      },
      colors: {
        primary: '#2C8BB3',
        primarydark: '#1A536B',
        primarydarker: '#123848',
        primarylight: '#A9D6EA',
        primarylighter: '#ECF6FA',
        secundary: '#E5E35F',
        brand_black: '#00090D',
        brand_white: '#FCFEFF',
      },
      width: {
        'mobile': '91.666667%',
        'desktop': '1140px',
      },
      maxWidth: {
        'mobile': '540px', 
        'tablet': '720px', 
        'desktop': '960px',
      },
      fontSize: {
        'h1': ['50px', {
          letterSpacing: '2.27px',
          lineHeight: '60px',
        }],
        'h1-mobile': ['38px', {
          letterSpacing: '1.73px',
          lineHeight: '46px',
        }],
        'h2': ['28px', {
          letterSpacing: '1.27px',
          lineHeight: '34px',
        }],
        'h2-mobile': ['21px', {
          letterSpacing: '0.95px',
          lineHeight: '25px',
        }],
        'p': ['16px', {
          letterSpacing: '0.8px',
          lineHeight: '24px',
        }],
        'p-mobile': ['12px', {
          letterSpacing: '0.6px',
          lineHeight: '18px',
        }],
        'p-small': ['10px', {
          letterSpacing: '0.45px',
          lineHeight: '15px',
        }],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('tailwindcss-debug-screens'),
  ],
}
